\select@language {brazilian}
\contentsline {chapter}{Fragmentos do diário de Adão}{11}
\contentsline {chapter}{Diário de Eva}{25}
\contentsline {section}{Fragmento do diário de Adão}{38}
\contentsline {section}{Fragmento do diário de Eva}{40}
\contentsline {section}{Depois da queda}{43}
\contentsline {section}{Quarenta anos depois}{46}
\contentsline {section}{No túmulo de Eva}{46}
\contentsline {chapter}{Passagens do diário de Satã}{47}
\contentsline {chapter}{Autobiografia de Eva}{55}
\contentsline {subsection}{Diário de Eva}{55}
\contentsline {chapter}{Solilóquio de Adão}{91}
\contentsline {chapter}{Posfácio}{101}
\contentsline {section}{\textsc {\MakeTextLowercase {AS}} \textsc {\MakeTextLowercase {AVENTURAS}} \textsc {\MakeTextLowercase {DE}} \textsc {\MakeTextLowercase {MARK}} \textsc {\MakeTextLowercase {TWAIN}}}{102}
\contentsline {section}{\textsc {\MakeTextLowercase {SONHO}} \textsc {\MakeTextLowercase {DE}} \textsc {\MakeTextLowercase {INFÂNCIA}}}{104}
\contentsline {section}{\textsc {\MakeTextLowercase {UMA}} \textsc {\MakeTextLowercase {TACADA}} \textsc {\MakeTextLowercase {DE}} \textsc {\MakeTextLowercase {SORTE}} (de Sam a Twain)}{106}
\contentsline {section}{O \textsc {\MakeTextLowercase {PALESTRANTE}} E O \textsc {\MakeTextLowercase {INVESTIDOR}}}{110}
\contentsline {section}{O \textsc {\MakeTextLowercase {EXÍLIO}}}{115}
\contentsline {section}{A \textsc {\MakeTextLowercase {IRONIA}} E A \textsc {\MakeTextLowercase {SÁTIRA}} \textsc {\MakeTextLowercase {EM}} \textsc {\MakeTextLowercase {TWAIN}}}{117}
\contentsline {section}{\textsc {\MakeTextLowercase {GÊNESE}} E \textsc {\MakeTextLowercase {PUBLICAÇÃODAS}} \textsc {\MakeTextLowercase {SÁTIRAS}} \textsc {\MakeTextLowercase {DA}} \textsc {\MakeTextLowercase {BÍBLIA}}}{121}
\contentsline {section}{A \textsc {\MakeTextLowercase {CAIXA}}"-\textsc {\MakeTextLowercase {PRETA}}}{122}
\contentsline {section}{O \textsc {\MakeTextLowercase {DIÁRIO}} E \textsc {\MakeTextLowercase {OUTROS}} \textsc {\MakeTextLowercase {CONTOSDE}} \textsc {\MakeTextLowercase {TEMÁTICA}} \textsc {\MakeTextLowercase {BÍBLICA}}}{126}
\contentsline {section}{\textsc {\MakeTextLowercase {BIBLIOGRAFIA}}}{130}
\contentsfinish 
